---
layout: markdown_page
title: "Diversity and Inclusion Events"
---

## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction







Summary of Events for 2020:

| Month    | Events                                                                                                                        |
|----------|-------------------------------------------------------------------------------------------------------------------------------|
| Apr 2019 | Hired D&I Manager                                                                                    |
| May 2019 | [Misc](https://about.gitlab.com/company/culture/inclusion/)                            |
| Jun 2019 | Misc                                     |
|          | Misc                                                       |
| Jul 2019 | [Misc](https://about.gitlab.com/company/culture/inclusion/)                         |
|          | [Misc](https://about.gitlab.com/company/culture/inclusion/)                                       |

